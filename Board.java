import edu.princeton.cs.algs4.Stack;

public class Board {
    // create a board from an n-by-n array of tiles,
    // where tiles[row][col] = tile at (row, col)
    private final int[][] board;
    private final int N;

    public Board(int[][] tiles) {
        this.N = tiles.length;
        this.board = tiles;
    }

    // string representation of this board
    public String toString() {
        StringBuilder s = new StringBuilder();
        int n = dimension();
        s.append(n + "\n");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                s.append(String.format("%2d ", this.board[i][j]));
            }
            s.append("\n");
        }
        return s.toString();
    }

    // board dimension n
    public int dimension() {
        return this.N;

    }

    // number of tiles out of place
    //note that 0 ain't a tile
    public int hamming() {
        int hamming = 0;
        for (int i = 0; i < this.N; i++) {
            for (int j = 0; j < this.N; j++) {
                if (this.board[i][j] == 0) {
                    continue;
                } else if (this.board[i][j] != this.N * i + j + 1)
                    hamming += 1;
            }
        }
        return hamming;
    }

    // sum of Manhattan distances between tiles and goal
    public int manhattan() {
        int manhattan = 0;
        for (int i = 0; i < this.N; i++) {
            for (int j = 0; j < this.N; j++) {
                if (this.board[i][j] == 0)
                    //go to next iteration of the loop
                    continue;
                else {
                    int ival;
                    if (this.board[i][j] % this.N == 0) {
                        ival = (this.board[i][j] / this.N) - 1;
                    } else {
                        ival = this.board[i][j] / this.N;
                    }
                    int jval = this.board[i][j] - 1 - this.N * ival;
                    manhattan += Math.abs(i - ival) + Math.abs(j - jval);
                }

            }
        }
        return manhattan;
    }

    // is this board the goal board?
    public boolean isGoal() {
        boolean a = manhattan() == 0;
        boolean b = hamming() == 0;

        return a && b;
    }

    // does this board equal y?
    //java inherited
    public boolean equals(Object y) {
        if (y == this) return true;
        if (y == null) return false;
        if (y.getClass() != this.getClass()) return false;

        Board that = (Board) y;
        if (this.dimension() != that.dimension()) return false;
        for (int i = 0; i < this.N; i++) {
            for (int j = 0; j < this.N; j++) {
                if (this.board[i][j] != that.board[i][j])
                    return false;
            }
        }
        return true;
    }

    // all neighboring boards
    public Iterable<Board> neighbors() {
        Stack<Board> s = new Stack<>();


        int[][] copy = arrayClone();

        int ival, jval;
        for (int i = 0; i < dimension(); i++) {
            for (int j = 0; j < dimension(); j++) {
                if (copy[i][j] == 0) {
                    ival = i;
                    jval = j;

                    if (ival + 1 < dimension()) {
                        copy = swap(copy, ival, jval, ival + 1, jval);
                        Board b = new Board(copy);

                        s.push(b);

                    }
                    if (ival - 1 >= 0) {
                        copy = arrayClone();
                        copy = swap(copy, ival, jval, ival - 1, jval);
                        Board b = new Board(copy);
                        s.push(b);
                    }
                    if (jval + 1 < dimension()) {
                        copy = arrayClone();
                        copy = swap(copy, ival, jval, ival, jval + 1);
                        Board b = new Board(copy);
                        s.push(b);
                    }
                    if (jval - 1 >= 0) {
                        copy = arrayClone();
                        copy = swap(copy, ival, jval, ival, jval - 1);
                        Board b = new Board(copy);
                        s.push(b);
                    }
                    copy = arrayClone();

                }
            }
        }
        return s;
    }

    private int[][] swap(int[][] a, int i, int j, int k, int z) {
        int swap;
        if (a[i][j] == 0)
            swap = 0;
        else
            swap = a[i][j];
        a[i][j] = a[k][z];
        a[k][z] = swap;
        return a;
    }

    private int[][] arrayClone() {
        int[][] a = new int[dimension()][dimension()];
        for (int i = 0; i < dimension(); i++) {
            for (int j = 0; j < dimension(); j++) {
                a[i][j] = this.board[i][j];

            }
        }
        return a;

    }


    // a board that is obtained by exchanging any pair of tiles
    public Board twin() {

        int[][] copy = arrayClone();

        if (copy[0][0] != 0 && copy[0][1] != 0) {
            copy = swap(copy, 0, 0, 0, 1);
            Board b = new Board(copy);
            return b;
        } else {
            copy = swap(copy, 1, 0, 1, 1);
            Board b = new Board(copy);
            return b;

        }
    }

    // unit testing (not graded)
    public static void main(String[] args) {
        int N = 3;
        int[][] chichi = new int[N][N];
        chichi[0][0] = 2;
        chichi[0][1] = 1;
        chichi[0][2] = 3;
        chichi[1][0] = 4;
        chichi[1][1] = 0;
        chichi[1][2] = 5;
        chichi[2][0] = 7;
        chichi[2][1] = 8;
        chichi[2][2] = 6;

        Board b = new Board(chichi);
        String rep = b.toString();
        System.out.println(rep);
        System.out.println("manhatten: " + b.manhattan() + " hamming: " + b.hamming());
        System.out.println("neighbours: ");
        for (Board c : b.neighbors())
            System.out.println(c.toString());

        System.out.println("twin: ");
        System.out.println(b.twin().toString());
    }

}
