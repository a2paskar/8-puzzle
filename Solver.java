import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.StdOut;


public class Solver {

    private final MinPQ<SearchNode> pq;
    private int num_moves;
    private final Board init;
    private final Stack<Board> solution = new Stack<Board>();
    private boolean twin;


    private class SearchNode implements Comparable<SearchNode> {
        Board curr;
        int num_moves;
        int priority;
        SearchNode prev;

        public SearchNode(Board curr, int num_moves, SearchNode prev) {
            this.curr = curr;
            this.num_moves = num_moves;
            this.prev = prev;
            this.priority = this.num_moves + this.curr.manhattan();
        }

        public int compareTo(SearchNode that) {
            return Integer.compare(this.priority, that.priority);
        }
    }

    // find a solution to the initial board (using the A* algorithm)
    public Solver(Board initial) {
        if (initial == null)
            throw new IllegalArgumentException();

        this.num_moves = 0;
        this.init = initial;

        Board twin = this.init.twin();
        MinPQ<SearchNode> twin_pq = new MinPQ<>();

        SearchNode twin_node = new SearchNode(twin, 0, null);
        twin_pq.insert(twin_node);

        SearchNode curr_node = new SearchNode(initial, 0, null);
        this.pq = new MinPQ<>();
        this.pq.insert(curr_node);
        this.twin = false;

        while (true) {

            if (twin_pq.min().curr.isGoal()) {
                this.twin = true;
                this.num_moves = -1;
                break;
            }


            if (this.pq.min().curr.isGoal()) {
                SearchNode goal = pq.delMin();
                this.num_moves = goal.num_moves;

                while (goal != null) {
                    this.solution.push(goal.curr);
                    goal = goal.prev;
                }
                break;

            }


            SearchNode chichi = pq.delMin();

            SearchNode cc = twin_pq.delMin();


            for (Board c : chichi.curr.neighbors()) {

                if (!c.equals(chichi.prev)) {
                    pq.insert(new SearchNode(c, chichi.num_moves + 1, chichi)); //increment num moves then add node

                }
            }

            for (Board c : cc.curr.neighbors()) {

                if (!c.equals(cc.prev)) {
                    twin_pq.insert(new SearchNode(c, cc.num_moves + 1, cc)); //increment num moves then add node
                }
            }


        }
    }

    // is the initial board solvable? (see below)
    public boolean isSolvable() {
        return !this.twin;
    }

    // min number of moves to solve initial board
    public int moves() {
        return this.num_moves;
    }

    // sequence of boards in a shortest solution
    public Iterable<Board> solution() {
        if (this.twin)
            return null;
        else
            return this.solution;
    }

    // test client (see below)
    public static void main(String[] args) {


        // create initial board from file
//        In in = new In(args[0]);
//        int n = in.readInt();
//        int[][] tiles = new int[n][n];
//        for (int i = 0; i < n; i++)
//            for (int j = 0; j < n; j++)
//                tiles[i][j] = in.readInt();
//        Board initial = new Board(tiles);

        // solve the puzzle
        int N = 3;
        int[][] chichi = new int[N][N];
        chichi[0][0] = 1;
        chichi[0][1] = 2;
        chichi[0][2] = 3;
        chichi[1][0] = 0;
        chichi[1][1] = 7;
        chichi[1][2] = 6;
        chichi[2][0] = 5;
        chichi[2][1] = 4;
        chichi[2][2] = 8;

        Board initial = new Board(chichi);

        Solver solver = new Solver(initial);

        // print solution to standard output
        if (!solver.isSolvable())
            StdOut.println("No solution possible");
        else {
            StdOut.println("Minimum number of moves = " + solver.moves());
            for (Board board : solver.solution())
                StdOut.println(board);
        }
    }
}
